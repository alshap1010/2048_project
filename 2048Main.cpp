#include "2048Square.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

//using namespace std;
int main() {
	Square theboard;
	srand(time(0));
	theboard.add_num();
	std::cout << theboard;

	while (theboard.check_largest() && (theboard.notfull() || theboard.can_horizontal() || theboard.can_vertical())) {
		Square check = theboard;
		char input;
		std::cin >> input;
		if (input == 'w') { theboard.goup(); }
		else if (input == 'a') { theboard.goleft(); }
		else if (input == 's') { theboard.godown(); }
		else if (input == 'd') { theboard.goright(); }
		if (theboard != check) { theboard.add_num(); }

		std::cout << theboard << std::endl;
		std::cout << "Current Score: " << theboard.get_score() << std::endl << std::endl;

	}

	if (!theboard.check_largest()) {
		std::cout << "CONGRATULATIONS!! You reached 2048!\n";
		std::cin.get();
	}
	else {
		"Oh no! You are stuck! Better luck next time!\n";
		std::cin.get();
	}

	/*while (theboard.get_largest() < 2048) {
		theboard.add_num();
		char input;
		std::cout << "What direction? w = up; a = left; s = down; d = right\nChoice: ";
		//std::cin >> input;
		std::cin.get(input);
		bool asd = true;
		while(asd){
			if (input == 'w') { theboard.goup(); asd = false; }
			else if (input == 'a') { theboard.goleft(); asd = false; }
			else if (input == 's') { theboard.godown(); asd = false; }
			else if (input == 'd') { theboard.goright(); asd = false; }
			else {
				std::cout << "Please give a correct direction: ";
				std::cin.get(input);
			}
		}
		std::cout << theboard << std::endl;
		std::cout << "Current Score: " << theboard.get_score() << std::endl << std::endl;

	}
	*/
	return 0;
}