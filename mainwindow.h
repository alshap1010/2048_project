#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include<iostream>
#include <ctime>
#include <cstdlib>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    bool can_horizontal() const {

            for(size_t i = 0; i < 4; ++i) {
                for (size_t j = 0; j < 3; ++j) {
                    if (gameboard[4 * i + j] == gameboard[4 * i + j + 1])
                        return true;
                }
            }
            return false;
        }

    bool can_vertical() const {

        for (size_t i = 0; i < 3; ++i) {
            for (size_t j = 0; j < 4; ++j) {
                if (gameboard[4 * i + j ] == gameboard[4 * i + j + 4])
                    return true;
            }
        }
        return false;

    }

    bool notfull() const {

        for (size_t i = 0; i < 16; ++i)
            if (gameboard[i] == 0)
                return true;
        return false;

    }

    void add_num() {

        size_t num_empty(0);
        for (size_t i = 0; i < 16; ++i)
            if (gameboard[i] == 0) { ++num_empty; }

        size_t add_at = rand() % num_empty + 1;
        int num2add(0);
        if ((rand() % 10) != 9)
            num2add = 2;
        else
            num2add = 4;

        size_t empty(0), placeholder(0);
        while (empty < add_at) {
            if (gameboard[placeholder] == 0)
                ++empty;
            ++placeholder;
        }

        gameboard[placeholder - 1] = num2add;

    }

    void goleft() {
            auto left = [this]() {
                for (size_t i = 0; i < 4; ++i) {
                    for (size_t j = 0; j < 4; ++j) {
                        if (gameboard[4 * i + j] == 0) {
                            for (size_t k = j; k < 3; ++k) {
                                gameboard[4 * i + k] = gameboard[4 * i + k + 1];
                                gameboard[4 * i + k + 1] = 0;
                            }

                        }

                    }

                }
            };
            left(); left();
            for (size_t i = 0; i < 4; ++i) {
                for (size_t j = 0; j < 3; ++j) {
                    if (gameboard[4 * i + j] == gameboard[4 * i + j + 1]) {
                        gameboard[4 * i + j] *= 2;
                        gameboard[4 * i + j + 1] = 0;
                        score += gameboard[4 * i + j];
                    }
                }
            }
            left();
        }

        void goright() {
            auto right = [this]() {
                for (size_t i = 0; i < 4; ++i) {
                    for (size_t j = 0; j < 4; ++j) {
                        //std::cout << 4 * i + 3 - j << std::endl;
                        if (gameboard[4 * i + 3 - j] == 0) {
                            //std::cout << "j= " << j;
                            for (size_t k = j; k < 3; ++k) {
                                //std::cout << "   k =" <<k << std::endl << 4*i + 3 - k  << std::endl;

                                gameboard[4 * i + 3 - k] = gameboard[4 * i + 2 - k];
                                gameboard[4 * i + 2 - k] = 0;
                            }

                        }

                    }

                }
            };
            right(); right();
            for (size_t i = 0; i < 4; ++i) {
                for (size_t j = 0; j < 3; ++j) {
                    if (gameboard[4 * i + 3 - j] == gameboard[4 * i + 2 - j]) {
                        gameboard[4 * i + 3 - j] *= 2;
                        gameboard[4 * i + 2 - j] = 0;
                        score += gameboard[4 * i + 3 - j];
                    }
                }
            }
            right();
        }

        void goup() {
            auto up = [this]() {
                for (size_t j = 0; j < 4; ++j) {
                    for (size_t i = 0; i < 4; ++i) {
                        if (gameboard[4 * i + j] == 0) {
                            for (size_t k = i; k < 3; ++k) {
                                gameboard[4 * k + j] = gameboard[4 * (k + 1) + j];
                                gameboard[4 * (k + 1) + j] = 0;
                            }

                        }

                    }

                }
            };

            up(); up();
            for (size_t i = 0; i < 4; ++i) {
                for (size_t j = 0; j < 3; j++) {
                    if (gameboard[4 * j + i] == gameboard[4 * (j+1) + i]) {
                        gameboard[4 * j + i] *= 2;
                        gameboard[4 * (j + 1) + i] = 0;
                        score += gameboard[4 * j + i];
                    }
                }
            }
            up();
        }

        void godown() {
            auto down = [this]() {
                for (size_t j = 0; j < 4; ++j) {
                    for (size_t i = 0; i < 4; ++i) {
                        if (gameboard[4 * (3 - i) + j] == 0) {
                            for (size_t k = i; k < 3; ++k) {
                                gameboard[4 * (3 - k) + j] = gameboard[4 * (2 - k) + j];
                                gameboard[4 * (2 - k) + j] = 0;
                            }

                        }

                    }

                }
            };
            down(); down();
            for (size_t i = 0; i < 4; ++i) {
                for (size_t j = 0; j < 3; j++) {
                    if (gameboard[4 * (3 - j) + i] == gameboard[4 * (2 - j) + i]) {
                        gameboard[4 * (3 - j) + i] *= 2;
                        gameboard[4 * (2 - j) + i] = 0;
                        score += gameboard[4 * (3 - j) + i];
                    }
                }
            }
            down();
        }
    size_t get_score()const { return score; }

    std::vector<int> get_vector() const { return gameboard;}

    int get_largest() const {
            int largest(gameboard[0]);
            for (size_t i = 1; i < 16; ++i)
                if (largest < gameboard[i]) largest = gameboard[i];
            return largest;
        }

    bool operator!=(const MainWindow& check) const {
            for (size_t i = 0; i < 16; ++i) {
                if (this->gameboard[i] != check.gameboard[i]) {
                    return true;
                }
            }
            return false;
        }

    void set_window();
    void set_color(QWidget* button, size_t index);

private:
    Ui::MainWindow *ui;
    std::vector<int> gameboard;
    size_t score;

public: //maybe should be a slot
    void keyPressEvent(QKeyEvent *event);
};

#endif // MAINWINDOW_H
