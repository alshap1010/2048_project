# README #

### Note ###

Most of this project was implemented before creating a repository. Obviously not the wisest choice. The first commit
adds images to show that the project was started multiple weeks ago and wasn't just a last minute project made during finals week.

### Which files for which? ###

The files to run on Microsoft Visual Studios or another ODE (these are more to show progress, but will run correctly):

-2048Square.h

-2048Main.cpp


The files to run with Qt graphics (This is the actual final project):

-2048Project.pro

-main.cpp

-mainwindow.cpp

-mainwindow.h

-mainwindow.ui


### How to Play ###
Press a (left), s (down), d (right), or w (up) to choose a direction to shift the values in the squares, and they will move either to the selected side, or as far towards the selected side until the hit another value. If there are two consecutive squares with the same value and in the same selected direction, they will merge into a single square with double the value. This resulting square will be the in the position of the square that is farther along in the selected direction. The score adds the value of every merged square.

### The goal ###

GET 2048!!

### Implementation Details ###

The data in the 2048 Square is held in a vector<int> with a size of 16.

An initial number, either a 2 or 4, is added to a random square. Then, a random 2 or 4 is added to a random empty square only after a valid shift from the user (i.e. they give a valid directional shift, then the shift is implemented, then the number is added to a random empty square).
To determine the random location, the program finds out the number of empty squares, uses rand()%(number of empty spots) to get a random number less than the number of empty spots, and then increments through the vector. If the vector spot if empty, a counter gets incremented, and once this counter equals the random number, the number gets added to the empty spot.

When the User input a shift direction, the program first shifts the values in the direction. For example, if 'a' is selected, all values get shifted to the left direction of the 4 by 4 2048 square. This function is implemented using a lambda function that captures "this" by reference to change the vector. Then, if there are consecutive squares with the same value and in the same direction as the inputted direction (e.g. left and right are horizontal, up and down are vertical) then then they are merged into a single square, and one of the squares is emptied. Then the same lambda function is called to make sure they are all shifted correctly. Lastly, a random 2 or 4 is added to a random empty square.

Whenever squares get merged, the score gets changes. There is a member size_t holding the score, and any merged values get added to the score.

Additionally, functions are written the determine if 2048 has been reached, or if the user is stuck and thus lost the game. To check if the user has lost, first the function notfull() checks if the square is full or not. If it is full, then it checks can_horizontal() and can_vertical() to see if there are consecutive square in any direction that have the same value. If there are, then, the user hasn't lost yet and can continue. Otherwise, there are no valid moves for the user to make, so they have lost.

The function operator != is also overloaded to check if the square has actually changed after user input. If square is already shifted to the left, and then the user inputs left, the square won't change, and a new random number should not be added to square.

### Qt Implementation details ###
The game runs mostly the same even under Qt; this section is more for have the graphics have been constructed.

The graphics were created using Qt Designer, and as such, the program is RAII compliant and will not have any memory leaks.

The graphics are made with many QPushbuttons and QLabels, because they appropriately match how the square should look.

User input comes from pressing the keyboard. The game is implemented to use the keys 'a', 's', 'd', and 'w'. The arrow keys already are connected to widgets, so to use the arrow keys, the program would have to first get rid of the connection and create a new one. Using letter keys was an easy method to get around this, since they don't already have a connection. Also, the keyboard signals are implemented differently, so they are caught even without having a separate slot manually implemented, which was another convenient was to make the program run. 

The program has a function set_window() that changes the labels in the graphics the the appropriate values from the vector. It also changes the colors of the edge of the QPushbuttons depending on the the value of the squares. This function is regularly used to update the graphics.