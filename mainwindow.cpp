#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), gameboard(std::vector<int>(16, 0)), score(0)
{
    this->add_num();

    ui->setupUi(this);
    this->set_window();
}

void MainWindow::keyPressEvent(QKeyEvent *event){
    std::vector<int> check = this->get_vector();
    if (event->key() == Qt::Key_W) { this->goup(); }
    else if (event->key() == Qt::Key_A) { this->goleft(); }
    else if (event->key() == Qt::Key_S) { this->godown(); }
    else if (event->key() == Qt::Key_D) { this->goright(); }
    if (this->get_vector() != check) { this->add_num(); }
    this->set_window();

}

void MainWindow::set_window(){
    if(this->get_largest() == 2048)
        ui->Outwinloss->setText("You Got 2048!");
    if( !(this->notfull()) && !this->can_horizontal() && !this->can_vertical() )
        ui->Outwinloss->setText("Oh no! You are stuck!");


    ui->pushButton_1->setText(QString::number(this->gameboard[0]));
    ui->pushButton_2->setText(QString::number(this->gameboard[1]));
    ui->pushButton_3->setText(QString::number(this->gameboard[2]));
    ui->pushButton_4->setText(QString::number(this->gameboard[3]));
    ui->pushButton_5->setText(QString::number(this->gameboard[4]));
    ui->pushButton_6->setText(QString::number(this->gameboard[5]));
    ui->pushButton_7->setText(QString::number(this->gameboard[6]));
    ui->pushButton_8->setText(QString::number(this->gameboard[7]));
    ui->pushButton_9->setText(QString::number(this->gameboard[8]));
    ui->pushButton_10->setText(QString::number(this->gameboard[9]));
    ui->pushButton_11->setText(QString::number(this->gameboard[10]));
    ui->pushButton_12->setText(QString::number(this->gameboard[11]));
    ui->pushButton_13->setText(QString::number(this->gameboard[12]));
    ui->pushButton_14->setText(QString::number(this->gameboard[13]));
    ui->pushButton_15->setText(QString::number(this->gameboard[14]));
    ui->pushButton_16->setText(QString::number(this->gameboard[15]));
    ui->Score_Label->setText(QString::number(this->score));
    this->set_color(ui->pushButton_1, 0);
    this->set_color(ui->pushButton_2, 1);
    this->set_color(ui->pushButton_3, 2);
    this->set_color(ui->pushButton_4, 3);
    this->set_color(ui->pushButton_5, 4);
    this->set_color(ui->pushButton_6, 5);
    this->set_color(ui->pushButton_7, 6);
    this->set_color(ui->pushButton_8, 7);
    this->set_color(ui->pushButton_9, 8);
    this->set_color(ui->pushButton_10, 9);
    this->set_color(ui->pushButton_11, 10);
    this->set_color(ui->pushButton_12, 11);
    this->set_color(ui->pushButton_13, 12);
    this->set_color(ui->pushButton_14, 13);
    this->set_color(ui->pushButton_15, 14);
    this->set_color(ui->pushButton_16, 15);
}

void MainWindow::set_color(QWidget* button, size_t index){

    QPalette pal = button->palette();
    pal.setColor(QPalette::Button, QColor(Qt::black));
    if(this->gameboard[index] == 2)
        pal.setColor(QPalette::Button, QColor(Qt::blue));
    else if(this->gameboard[index] == 4)
        pal.setColor(QPalette::Button, QColor(Qt::red));
    else if(this->gameboard[index] == 8)
        pal.setColor(QPalette::Button, QColor(Qt::green));
    else if(this->gameboard[index] == 16)
        pal.setColor(QPalette::Button, QColor(244, 185, 66));
    else if(this->gameboard[index] == 32)
        pal.setColor(QPalette::Button, QColor(49,249,239));
    else if(this->gameboard[index] == 64)
        pal.setColor(QPalette::Button, QColor(57,53,244));
    else if(this->gameboard[index] == 128)
        pal.setColor(QPalette::Button, QColor(244,34,104));
    else if(this->gameboard[index] == 256)
        pal.setColor(QPalette::Button, QColor(219,200,35));
    else if(this->gameboard[index] == 512)
        pal.setColor(QPalette::Button, QColor(75,234,101));
    else if(this->gameboard[index] == 1024)
        pal.setColor(QPalette::Button, QColor(66, 244, 149));
    else if(this->gameboard[index] == 2048)
        pal.setColor(QPalette::Button, QColor(255,30,232));

    button->setAutoFillBackground(true);
    button->setPalette(pal);
    button->update();

}

MainWindow::~MainWindow()
{
    delete ui;
}
